//
// Created by jose on 26/05/18.
//

#ifndef MILLERRABIN_EUCLIDES_H
#define MILLERRABIN_EUCLIDES_H
int it;

inline int algoritmoEuclides(long a, long b){
    if(b==0)
        return static_cast<int>(a);
    it++;
        algoritmoEuclides(b,a%b);

}

inline int algoritmoEuclidesOpt(long a, long b){
    long r;
    it=0;
    while(b != 0){
        r = b;
        b = a % b;
        a = r;
        it++;
    }
    return static_cast<int>(a);
}

inline void algoritmoEuclidianoExtendido(long a, long b){
    it=0;
    long x,y,d;
    long x2 = 1, x1 = 0, y2 = 0, y1 = 1,q = 0, r = 0;

    if(b==0){
        d = a;	x = 1;	y = 0;
    }
    else{
        while(b>0){
            it++;
            q = (a/b);
            r = a - q*b;
            x = x2-q*x1;
            y = y2 - q*y1;
            a = b;
            b = r;
            x2 = x1;
            x1 = x;
            y2 = y1;
            y1 = y;
        }
        d=a;
        x=x2;
        y=y2;
    }
}


#endif //MILLERRABIN_EUCLIDES_H
