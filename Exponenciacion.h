//
// Created by jose on 25/05/18.
//

#pragma clang diagnostic push
#pragma ide diagnostic ignored "ClangTidyInspection"
#ifndef MILLERRABIN_BINARIZAR_H
#define MILLERRABIN_BINARIZAR_H
#define MAX 50


#include <cmath>
using namespace std;

int ki[MAX];
int l=0;
int n_exponeciacion;

inline void binarizar(int k){
    int x;
    x=k;
    do{
        ki[l]=x%2;
        x=x/2;
        l++;
    }while(x!=0);
}

inline int exponenciacion(int a, int k){

    int b=1,A=a;

    for (int &i : ki) {
        i =0;
    }
    binarizar(k);
    if(k==0){
        return b;
    }
    if(ki[0]==1){
            b=a;
        }
        for(int i=1;i<l;i++) {
            A = (A * A) % n_exponeciacion;
            if (ki[i] == 1) {
                b = (A * b) % n_exponeciacion;
            }
        }

    return b;
}

#endif //MILLERRABIN_BINARIZAR_H

#pragma clang diagnostic pop