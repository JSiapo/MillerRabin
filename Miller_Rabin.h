//
// Created by jose on 25/05/18.
//

#ifndef MILLERRABIN_MILLER_RABIN_H
#define MILLERRABIN_MILLER_RABIN_H
#include "Euclides.h"
#include "Exponenciacion.h"
#include<cstdlib>
#include<ctime>

int r,s;

void rs(int n)
{
    r=n-1;
    while(r%2==0)
    {
        r=r/2;
        s++;
    }
}
clock_t t1;
clock_t t0;
double time_exec;
bool miller_rabin(int n, long t){
    t0= static_cast<unsigned int>(clock());
    int a,y,j;
    srand(static_cast<unsigned int>(time(nullptr)));
    n_exponeciacion=n+1;
    rs(n);
    cout<<"\nr es "<<r;
    cout<<"\ns es "<<s<<endl;
    for (int i = 0; i <t; i++) {
        a= (2 + rand() % (n - 3)); // NOLINT
        cout<<"\na es "<<a;
        y=exponenciacion(a,r);
        cout<<"\ny es "<<y;
        if(y!=1&&y!=(n-1)){
                j=1;
                while (j<=(s-1)&& y!=(n-1)){
                    y= (y * y) % n;
                    cout<<"\n---y es "<<y;
                    if(y==1){
                        t1 = clock();
                        time_exec = (double(t1-t0)/CLOCKS_PER_SEC);
                        cout << "\nExecution Time: " << time_exec << endl;
                        return false;
                    }
                    y=j+1;
                }
                if(y!=(n-1)){
                    t1 = static_cast<unsigned int>(clock());
                    time_exec = (double(t1-t0)/CLOCKS_PER_SEC);
                    cout << "\nExecution Time: " << time_exec << endl;
                    return false;
                }
        }
    }
    t1 = clock();
    time_exec = (double(t1-t0)/CLOCKS_PER_SEC);
    cout << "\nExecution Time: " << time_exec << endl;
    return true;
}

#endif //MILLERRABIN_MILLER_RABIN_H
